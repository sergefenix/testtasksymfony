Тестовое задание должно быть выполнено на Symfony (версии 4.*).

Создать веб-приложение "Каталог авторов и книг" со следующими возможностями:
- Позволяет создавать/редактировать/удалять авторов в каталоге
- Позволяет создавать/просматривать/редактировать/удалять книги из каталога
- Позволяет связывать книги с авторами
- Позволяет увидеть список книг
- Позволяет увидеть список авторов

Требования к исполнению:
Требования к сущности "Автор":
- Должна быть представлен объектом
- Должна хранить ФИО автора
- При выводе в списке ФИО должна быть представлена в сокращенном формате (Фамилия + Инициалы)
- Повторное добавление в каталог существующего автора запрещено

Требование к сущности "Книга":
- Должна быть представлена объектом
- Должна хранить название книги, год издания книги, ISBN, Кол-во страниц
- Должна быть возможность связать с одной сущностью книги несколько сущностей авторов (1..n)
- Повторное добавление в каталог существующей (Название + год или ISBN) книги запрещено

Требования к каталогам:
- Каталоги должны хранить в БД (sqlite/mysql/postgresql)
Дополнительные требования:
Дополнительные требования к сущности "Книга":
- Возможность прикрепить изображение обложки книги, в формате jpeg или png, которое будет храниться в виде файла

Для запуска проекта 
- composer i  
- docker-compose up -d 
- php bin/console doctrine:migrations:migrate
 
