<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\BookType;
use App\Entity\Book;
use Exception;

class BookController extends AbstractController
{
    /**
     * @Route("/books", name="book")
     */
    public function index(): Response
    {
        $manager = $this->getDoctrine()->getManager();

        $books = $manager->getRepository(Book::class)->findAll();

        return $this->render('book/index.html.twig', [
            'books' => $books
        ]);
    }

    /**
     * @Route("/book/single/{book}", name="single_book")
     * @param Book $book
     *
     * @return Response
     */
    public function single(Book $book): Response
    {
        $destination = $this->getParameter('kernel.project_dir') . '/public/uploads/';

        return $this->render('book/single.html.twig', [
            'book'        => $book,
            'destination' => $destination
        ]);
    }

    /**
     * @Route("/book/create", name="create_book")
     * @param Request $request
     *
     * @return Response
     * @throws Exception
     */
    public function create(Request $request): Response
    {
        $book = new Book();

        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $book = $form->getData();
            $manager = $this->getDoctrine()->getManager();

            $uploadedFile = $form['image']->getData();
            $destination = $this->getParameter('kernel.project_dir') . '/public/uploads';
            $newName = bin2hex(random_bytes(6)) . '.' . $uploadedFile->guessExtension();

            $uploadedFile->move(
                $destination,
                $newName
            );

            $book->setImage($newName);

            $manager->persist($book);
            $manager->flush();

            return $this->redirectToRoute('book');
        }

        return $this->render('book/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/book/update/{book}", name="update_book")
     * @param Request $request
     *
     * @param Book    $book
     *
     * @return Response
     * @throws Exception
     */
    public function update(Request $request, Book $book): Response
    {
        $form = $this->createForm(BookType::class, $book, [
            'action' => $this->generateUrl('update_book', [
                'book' => $book->getId()
            ]),
            'method' => 'POST',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getDoctrine()->getManager();
            $manager->flush();

            $uploadedFile = $form['image']->getData();
            $destination = $this->getParameter('kernel.project_dir') . '/public/uploads';
            $newName = bin2hex(random_bytes(6)) . '.' . $uploadedFile->guessExtension();

            $uploadedFile->move(
                $destination,
                $newName
            );

            $book->setImage($newName);

            return $this->redirectToRoute('book');
        }

        return $this->render('book/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/book/delete/{book}", name="delete_book")
     * @param Book    $book
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function deleteBook(Book $book, Request $request): RedirectResponse
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($book);
        $manager->flush();

        return $this->redirectToRoute('book');
    }
}
