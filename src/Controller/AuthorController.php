<?php

namespace App\Controller;

use App\Entity\Author;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\AuthorType;

class AuthorController extends AbstractController
{

    /**
     * @Route("/", name="main")
     */
    public function main(): Response
    {
        return $this->render('main.html.twig');
    }

    /**
     * @Route("/authors", name="author")
     */
    public function index(): Response
    {
        $manager = $this->getDoctrine()->getManager();

        $authors = $manager->getRepository(Author::class)->findAll();

        return $this->render('author/index.html.twig', [
            'controller_name' => 'AuthorController',
            'authors'         => $authors
        ]);
    }

    /**
     * @Route("/author/create", name="create_author")
     * @param Request $request
     *
     * @return Response
     */
    public function create(Request $request): Response
    {
        $author = new Author();

        $form = $this->createForm(AuthorType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $author = $form->getData();
            $manager = $this->getDoctrine()->getManager();

            $manager->persist($author);
            $manager->flush();

            return $this->redirectToRoute('author');
        }

        return $this->render('author/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/author/update/{author}", name="update_author")
     * @param Author  $author
     * @param Request $request
     *
     * @return Response
     */
    public function update(Request $request, Author $author): Response
    {
        $form = $this->createForm(AuthorType::class, $author, [
            'action' => $this->generateUrl('update_author', [
                'author' => $author->getId()
            ]),
            'method' => 'POST',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getDoctrine()->getManager();
            $manager->flush();

            return $this->redirectToRoute('author');
        }

        return $this->render('author/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/author/delete/{author}", name="delete_author")
     * @param Author  $author
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function delete(Author $author, Request $request): RedirectResponse
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($author);
        $manager->flush();

        return $this->redirectToRoute('author');
    }
}
