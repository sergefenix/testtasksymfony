<?php

namespace App\Form;

use App\Entity\Author;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use App\Entity\Book;

class AuthorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Name', 'required' => true])
            ->add('surname', TextType::class, ['label' => 'Surname', 'required' => true])
            ->add('patronymic', TextType::class, ['label' => 'Patronymic', 'required' => true])
            ->add('books', EntityType::class, [
                'label'    => 'Books',
                'class'    => Book::class,
                'expanded' => true,
                'multiple' => true,
                'choice_label' => 'name',
            ])
            ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Author::class,
        ]);
    }
}
